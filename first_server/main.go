package main

import ( "net/http"
		 "time" 
		 "log")

func main() {

	StartServer()	
	log.Println("[INFO] Servidor no ar!")
}

func requests(w http.ResponseWriter, r *http.Request){

	j := r.Method
	pacote := r.URL.String()

	w.Write([]byte("Method: " + j + "	pacote: " + pacote))
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.25:8088",
			IdleTimeout: duration, 
	}

	http.HandleFunc("/", requests)

	log.Print(server.ListenAndServe())
}
